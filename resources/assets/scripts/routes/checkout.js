export default {
  init() {
    // JavaScript to be fired on the checkout page
    let $shippingInputs = $( '.shipping_address input, #order_comments' );
    let shippingIDs = [  ];

    $shippingInputs.each( function( i ) {
      shippingIDs[ i ] = $( this ).attr( 'id' );
      $( '.' + shippingIDs[i] ).text( $( this ).val(  ) );

      $( '#' + shippingIDs[i] ).on( 'input', function(  ) {
        $( '.' + shippingIDs[i] ).text( $( this ).val(  ) );
      } );
    } );

    let $billingInputs = $( '.woocommerce-billing-fields input' );
    let billindIDs = [  ];

    $billingInputs.each( function( i ) {
      billindIDs[ i ] = $( this ).attr( 'id' );
      $( '.' + billindIDs[i] ).text( $( this ).val(  ) );

      $( '#' + billindIDs[i] ).on( 'input', function(  ) {
        $( '.' + billindIDs[i] ).text( $( this ).val(  ) );
      } );
    } );

    /**
     * NAVIGATION
     */

    $( '.dg-checkout-navigation .prev, .checkout-steps-banner .step-back' ).on( 'click', function( e ) {
      e.preventDefault(  );
      $( '#dg-order-review' ).fadeOut( 'slow', (  ) => {
        $( '#customer_details' ).fadeIn(  );
      } );

      $( '.checkout-steps-banner .step-two' ).fadeOut( 'slow', (  ) => {
        $( '.checkout-steps-banner .step-one' ).fadeIn(  );
      } );

      $( 'html, body' ).animate( {
        scrollTop: $( 'form.checkout.woocommerce-checkout' ).offset(  ).top,
      }, 700 );
    } );

    $( '.dg-checkout-navigation .next' ).on( 'click', function( e ) {
      e.preventDefault(  );
      $( '#customer_details' ).fadeOut( 'slow', (  ) => {
        $( '#dg-order-review' ).fadeIn(  );
      } );

      $( '.checkout-steps-banner .step-one' ).fadeOut( 'slow', (  ) => {
        $( '.checkout-steps-banner .step-two' ).fadeIn(  );
      } );

      setTimeout( function(  ) {
        $( 'html, body' ).animate( {
          scrollTop: $( '#dg-order-review' ).offset(  ).top,
        }, 700 );
      }, 500 );
    } );

    $( '.zoid-component-frame.zoid-visible' ).on( 'load', (  ) => {
      let head = $( '.zoid-component-frame.zoid-visible' ).contents(  ).find( 'head' );
      let css = '<style>.paypal-button-label-container { display: none; }</style>';
      $( head ).append( css );
    } )
  },
};
