export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    $( '.term-glasses .product, .term-sunglasses .product' ).on( 'click', ( e ) => {
      e.preventDefault(  );
    } )
  },
};
