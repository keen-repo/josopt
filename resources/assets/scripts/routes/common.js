export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top,
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(':focus')) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }
          });
        }
      }
    });

    $( '.woocommerce-notices-wrapper' ).on( 'click', function(  ) {
      $( this ).fadeOut(  );
    } );

    $( '.title-text-read-more .condition' ).on( 'click', function( e ) {
      e.preventDefault(  );
      if ( $( this ).hasClass( 'open' ) ) {
        $( this ).removeClass( 'open' )
                  .find( '.text-full' ).slideUp();//.css( 'display', 'none' )
        //$( this ).find( '.text' ).fadeIn();//.css( 'display', 'block' );
      } else {
        $( this ).addClass( 'open' )
                  .find( '.text-full' ).slideDown();//.css( 'display', 'block' )
        //$( this ).find( '.text' ).css( 'display', 'none' );
      }
    } );

    let navDown = false;
    $( '.menu-toggle' ).on( 'click', () => {
      if ( navDown == false ) {
        $( '.main-navigation .nav' ).slideDown();
        navDown = true;
      } else if ( navDown == true ) {
        $( '.main-navigation .nav' ).slideUp();
        navDown = false;
      }
    });

    $( '.main-navigation .nav li.menu-item-has-children > a' ).one( 'click', function( e ) {
      let wW = $( window ).outerWidth(  );
      if ( wW < 1023 ) {
        e.preventDefault(  );
        if ( $( this ).next(  ).hasClass( 'open' ) ) {
          $( this ).next(  ).slideUp(  ).removeClass( 'open' );
        } else {
          $( this ).next(  ).slideDown(  ).addClass( 'open' );
        }
      }
    } );

    $( '.main-menu-open' ).on( 'click', function(  ) {
      $( '.main-navigation' ).fadeIn(  );
    } );

    $( '.main-navigation .btn-close' ).on( 'click', function(  ) {
      $( '.main-navigation' ).fadeOut(  );
    } );

    let params           = window.location.search.substring( 1 );
    if ( params ) {
      let urlVars          = params.split( '&' );
      let splitArr         = [  ];
      let filter           = [  ];
      let value            = [  ];

      for ( var i = 0; i < urlVars.length; i++ ) {
        splitArr = urlVars[ i ].split( '=' );
        filter = splitArr[ 0 ];
        value = splitArr[ 1 ];
        $( 'select.' + filter ).val( value )
      }
    }

    /**
     * The cart popup
     */
    let cartPopupOpen = false;
    $( '.login-register-menu .last-item, .dg-cart-popup .close' ).on( 'click', function( e ) {
      e.preventDefault(  );
      if ( !cartPopupOpen ) {
        $( '.dg-cart-popup' ).fadeIn(  );
        cartPopupOpen = true;
      } else {
        $( '.dg-cart-popup' ).fadeOut(  );
        cartPopupOpen = false;
      }
    } );

    $( '.dg-cart-popup .trash-product' ).on( 'click', function( e ) {
      e.preventDefault(  );
      let prodID = $( this ).data( 'id' );
      console.log( prodID );
      $.ajax({
        type: 'post',
        dataType: 'json',
        url: ajaxObj.ajaxurl,
        data: {
          action: 'update_cart',
          prod_id: prodID,
        },
        success: function( response ) {
            console.log( response );
          if ( response.type == 'success' ) {
            console.log( 'success' );
            $( '.jos-product-id-' + response.id ).closest(  ).remove(  );
          } else {
            console.log( 'else no success' );
          }
        },
      });
    } );
    // The cart popup - END

    $( '.login-register-menu .logged-out' ).on( 'click', function( e ) {
      e.preventDefault(  );
      $( '.dg-wc-login-form' ).fadeIn(  );
    } );

    $( '.dg-wc-login-form .close' ).on( 'click', function( e ) {
      e.preventDefault(  );
      $( '.dg-wc-login-form' ).fadeOut(  );
    } );

    /**
     * Accordion setup
     */
    let questionWrapper = $( '.jos-qa' );
    let question = $( '.jos-qa .question' );
    let answers = $( '.jos-qa .answer' );
    question.on( 'click', openSibling );

    //let setTitle = $( '.links-sets .link-set-title' );
    //let setLinks = $( '.links-sets .link-set' );
    //setTitle.on( 'click', openSibling );

    function openSibling(  ) {
      // if the current answer is open
      if ( $( this ).next().hasClass( 'open' ) ) {
        // close it up, add class 'hide' and remove class 'open'
        $( this ).next().slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
        $( this ).parent( ).removeClass( 'current' );
        $( this ).removeClass( 'current' );
      } else {
        // otherwise, close all answers up, add class 'hide' and remove class 'open'
        //setLinks.slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
        answers.slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
        // then we open the answer to the question clicked
        $( this ).next( ).slideDown(  ).removeClass( 'hide' ).addClass( 'open' );
        questionWrapper.removeClass( 'current' );
        question.removeClass( 'current' );
        $( this ).parent(  ).addClass( 'current' );
        $( this ).addClass( 'current' );
      }
    }

    // Iterate over each select element
    $('select').each(function() {
      // Cache the number of options
      var $this = $(this),
          numberOfOptions = $(this).children('option').length;
      // Hides the select element
      $this.addClass('s-hidden');
      // Wrap the select element in a div
      $this.wrap('<div class="select"></div>');
      // Insert a styled div to sit over the top of the hidden select element
      $this.after('<div class="styledSelect"></div>');
      // Cache the styled div
      var $styledSelect = $this.next('div.styledSelect');
      // Show the first select option in the styled div
      $styledSelect.text($this.children('option:selected').text());
      // Insert an unordered list after the styled div and also cache the list
      var $list = $('<ul />', {
          'class': 'options',
      }).insertAfter($styledSelect);
      // Insert a list item into the unordered list for each select option
      for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
          text: $this.children('option').eq(i).text(),
          rel: $this.children('option').eq(i).val(),
          'data-search': $this.children('option').eq(i).data('search'),
          'data-searchmin': $this.children('option').eq(i).data('searchmin'),
          'data-searchminval': $this.children('option').eq(i).data('searchminval'),
          'data-searchmaxval': $this.children('option').eq(i).data('searchmaxval'),
        }).appendTo($list);
      }
      // Cache the list items
      var $listItems = $list.children('li');
      // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
      $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.styledSelect.active').each(function() {
          $(this).removeClass('active').next('ul.options').hide();
        });
        $(this).toggleClass('active').next('ul.options').toggle();
      });

      $styledSelect.bind( 'DOMSubtreeModified', function(  ) {
        //let selectContext = $( '.select_desired-time' );
        $( this ).closest( '.select_desired-time' ).find( '.frm_primary_label' ).text( '' );
      } )
      // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
      // Updates the select element to have the value of the equivalent option
      $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();

        //let $ddata = $( this ).data(  );


        let filter          = $( this ).data( 'search' );
        let selectedOption  = $( this ).attr( 'rel' );
        let newParams       = '';
        let baseURL          = window.location.protocol + '//' + window.location.host + window.location.pathname;
        // get the parameters after the '/'
        let params           = window.location.search.substring( 1 );
        // decode 'params'
        let decode           = decodeURI( params );
        // split parameters into an array at every '&'
        let urlVars          = decode.split( '&' );
        // split parameters into an array at every '='
        let splitVars        = [  ];
        let splitArr         = [  ];
        let varsObj          = {};

        for ( var i = 0; i < urlVars.length; i++ ) {
          splitArr = urlVars[i].split( '=' )
          splitVars.push( splitArr );
          let firstIndex = splitArr[ 0 ];
          let secondIndex = splitArr[ 1 ];
          varsObj[ firstIndex ] = secondIndex;
          delete varsObj[ filter ];
          $( 'select.' + filter ).val( secondIndex )
        }

        //newParams = $.param( varsObj ) + '&';

        $.each( varsObj, function( key, value ) {
          if ( key ) {
            newParams += key + '=' + value + '&';
          }
        } );

        switch ( filter ) {
          case 'minmax1' :
              filter = 'min_price=10&max_price'
            break;
          case 'minmax2' :
              filter = 'min_price=100&max_price'
            break;
          case 'minmax3' :
              filter = 'min_price=500&max_price'
            break;
        }

        let finalParams = '?' + newParams + filter + '=' + selectedOption;
        let finalURL = finalURL = baseURL + finalParams;

        //update select with value from styledSelect
        $this.trigger('change');

        if ( filter ) {
          console.log( finalURL )
          document.location = finalURL;
        }
          /* alert($this.val()); Uncomment this for demonstration! */
      });
      // Hides the unordered list when clicking outside of it
      $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
      });
    });

    $('.reset-filters').click(function(){
      $('.jos-category-filters .styledSelect').each(function(){
        var $this = $(this);
        $this.text($this.siblings('select').children('option').eq(0).text());
      });

      window.location.href = document.location.href.split('?')[0];
    });


    setTimeout( function( ) {
      doHeroSlider(  );
      doHeroImage(  );
      doProductsSlider(  );
      doProductsCollectionsCategoriesSlider(  );
      doTestimonialsSlider(  );
      doImageSlider(  );
      removePreLoader(  );
    }, 500 );

    $( window ).on( 'resize', doHeroImage );
  },
};

function removePreLoader(  ) {
  $( '.pre-loader-wrap' ).fadeOut( 'slow' );
}

function doImageSlider(  ) {
  var settings = {
    dots: false,
    arrows: true,
    nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 1500,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  $( '.image-slider .images-slider' ).slick( settings );
}

function doHeroImage(  ) {
  let wW                  = $( window ).outerWidth(  );
  //let wH                  = $( window ).outerHeight(  );
  let bannerHeight        = $( '.banner' ).outerHeight(  );
  let thumbHeight         = $( '.post-thumbnail' ).outerHeight(  );
  let heroHeight          = ( thumbHeight - bannerHeight );
  let wrapContainer       = $( '.wrap.container' );

  wrapContainer.css( 'margin-top', heroHeight + 'px' );
  // remove calculated margin on the home page
  $( '.home .wrap.container' ).css( 'margin-top', '' );
  // remove calculated margin on the checkout page
  $( '.checkout .wrap.container' ).css( 'margin-top', '' );
  // remove calculated margin on the thank you page
  $( '.template-thank-you .wrap.container' ).css( 'margin-top', '' );
  // remove calculated margin on the 404 page
  $( '.error404 .wrap.container' ).css( 'margin-top', '' );

  if ( wW >= 1024) {
    $( '.main-navigation' ).fadeIn(  );
  } else {
    $( '.main-navigation' ).hide(  );
  }
}

function doHeroSlider(  ) {
  var heroSlider = {
    dots: false,
    arrows: false,
    //nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    //prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    fade: true,
    speed: 700,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.hero-slides-content',
  };

  var heroSliderContent = {
    dots: true,
    arrows: false,
    //nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    //prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.hero-slides',
  };

  $( '.hero-slides' ).slick( heroSlider );
  $( '.hero-slides-content' ).slick( heroSliderContent );
}

function doProductsSlider(  ) {
  var theSlider = {
    dots: false,
    arrows: true,
    nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1042,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  let $slides = $( '.products-slides .woocommerce ul.products' );

  $slides.on( 'init', function(  ) {
    let $lastSlide = $( '.products-slides .slick-active' ).last(  );
    $lastSlide.addClass( 'last-active-slide' );
    console.log( 'init' )
  } );

  $slides.on( 'beforeChange', function(  ) {
    $( '.products-slides .slick-slide' )
      .removeClass( 'last-active-slide' )
      .addClass( 'inactive-slide' );
  } );

  $slides.on( 'afterChange', function(  ) {
    let $lastSlide = $( '.products-slides .slick-active' ).last(  );
    $lastSlide.addClass( 'last-active-slide' );
  } );

  $slides.slick( theSlider );
}

function doProductsCollectionsCategoriesSlider(  ) {
  var settings = {
    dots: false,
    arrows: true,
    nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1042,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  $( '.products-collections-by-categories .images' ).slick( settings );
  $( '.products-collections-by-brand .images' ).slick( settings );
}

function doTestimonialsSlider(  ) {
  var settings = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  $( '.testimonials-slider' ).slick( settings );
}
