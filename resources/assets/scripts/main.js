// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// Import Slick
import 'slick-carousel/slick/slick.min';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import checkout from './routes/checkout';
import woocommerce from './routes/woocommerce';
import singleProduct from './routes/mobile-check';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  checkout,
  woocommerce,
  singleProduct,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
