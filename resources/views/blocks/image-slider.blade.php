{{--
  Title: Image Slider
  Description: Image slider
  Category: dg_block_category
  Icon: admin-comments
  Keywords: image, slider
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$images = $flds[ 'images' ];

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image,
  'no_dots'         => ''
];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="images-slider">
    @foreach ( $images as $image )
      @php
      $img      = $image[ 'image' ];
      $imgURL   = $img[ 'url' ];
      $image    = aq_resize( $imgURL, 789, 547, true, true, true );
      $alt      = $img[ 'alt' ];
      @endphp
      <div class="image-slide"><img src="{{ $image }}" alt="{{ $alt }}"></div>
    @endforeach
  </div>
@endcomponent
