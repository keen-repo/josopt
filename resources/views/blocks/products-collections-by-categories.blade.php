{{--
  Title: Products Collections by Categories
  Description: Products collection image
  Category: dg_block_category
  Icon: admin-comments
  Keywords: product, collection, images, category, categories
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image
];

$cs = $flds[ 'select_product_category' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="images">
    @foreach ( $cs as $c )
      @php
      $catID = $c->term_id;
      $thumbID = get_term_meta( $catID, 'thumbnail_id', true );
      $image = wp_get_attachment_url( $thumbID );
      $img = aq_resize( $image, 382, 461, true, true, true );
      $btnLink = get_term_link( $catID, 'product_cat' );
      $btnTitle = 'view collection';
      @endphp
      <div class="collection-wrapper">
        <div class="image">
          <img src="{{ $img }}" alt="">
        </div>
        <a href="{{ $btnLink }}" class="overlay"></a>
        @include ( 'partials/buttons/blue-btn' )
      </div>
    @endforeach
  </div>
@endcomponent
