{{--
  Title: Products Slider
  Description: Products slider
  Category: dg_block_category
  Icon: admin-comments
  Keywords: products, slider, shop, online
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}
@include ( 'partials.bobs.products-slider-posts' )
{{--
  This block is used on single posts. So we call it, to keep code DRY
--}}
