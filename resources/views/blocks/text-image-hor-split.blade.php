{{--
  Title: Text / Image Horizontal Split
  Description: Text on the left / Image on the right
  Category: dg_block_category
  Icon: admin-comments
  Keywords: text, image, left, right
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$direction = $flds[ 'direction' ];
$sections = $flds[ 'text_image_section' ];

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$direction}",
  'title'           => $sectionTitle,
  'backImg'         => $image,
  'no_dots'         => ''
];
@endphp

@component( 'components.blocks', $componentVars )
  @foreach ( $sections as $section )
    @php
    $title = $section[ 'title' ];
    $text = $section[ 'text' ];
    $image = $section[ 'image' ];
    $img = $image[ 'url' ];
    $img = aq_resize( $img, 960, 523, true, true, true );
    $alt = $image[ 'alt' ];
    @endphp
    <div class="title-image-section {{ $loop->index % 2 == 0 ? 'column-even' : 'column-odd' }}">
      <div class="text-section">
        <div class="text-wrapper">
          <h3 class="title">{{ $title }}</h3>
          <div class="text">{!! $text !!}</div>
        </div>
      </div>
      <div class="image-section" style="background-image: url( '{{ $img }}' );"></div>
    </div>
  @endforeach
@endcomponent
