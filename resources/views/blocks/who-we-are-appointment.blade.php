{{--
  Title: Who we are / Appointment
  Description: Who we are / Appointment
  Category: dg_block_category
  Icon: admin-comments
  Keywords: who, we, are, appointment
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];
$secProp2 = $flds[ 'sec_prop_2' ];

$activeWhoWeAre = $secProp[ 'active' ];
$activeAppointment = $secProp2[ 'active' ];

$titleWhoWeAre = $secProp[ 'section_title' ];
$titleAppointment = $secProp2[ 'section_title' ];

$whoImage = $flds[ 'image' ];

$topText = $flds[ 'top_text' ];
$whoWeAreText = $flds[ 'who_we_are_text' ];

if ( $activeAppointment ) {
  $img = aq_resize( $whoImage, 960, 865, true );
} else {
  $img = aq_resize( $whoImage, 1920, 865, true );
}

$btn        = $flds[ 'button' ];
if ( $btn ) {
  $btnTitle      = $btn[ 'title' ];
  $btnLink       = $btn[ 'url' ];
  $btnTarget     = $btn[ 'target' ];
}
@endphp

@if ( $activeWhoWeAre || $activeAppointment )
  <section id="{{ $block[ 'id' ] }}" class="keen-block {{ $block[ 'classes' ] }}">
    <div id="book-an-appointment"></div>
    @if ( $activeWhoWeAre )
      <div class="{{ $block[ 'classes' ] }}-wrapper who-we-are{{ $activeAppointment ? '' : ' full-width' }}" style="background-image: url( '{{ $img }}' );">
        <div class="full-width-wrapper">
          <div class="blur-wrap">
            <h2 class="section-title">{{ $titleWhoWeAre }}</h2>
            @include ( 'partials.bobs.blue-dots' )
            @if ( $whoWeAreText )
              <div class="text">{{ $whoWeAreText }}</div>
            @endif
            @include ( 'partials/buttons/blue-btn' )
          </div>
        </div>
      </div>
    @endif

    @if ( $activeAppointment )
      <div class="{{ $block[ 'classes' ] }}-wrapper make-an-appointment{{ $activeWhoWeAre ? '' : ' full-width' }}">
        <div class="full-width-wrapper">
          <h2 class="section-title">{{ $titleAppointment }}</h2>
          @include ( 'partials.bobs.blue-dots' )
          <div class="top-text">{{ $topText }}</div>
          @php
          echo do_shortcode( "[formidable id={$flds[ 'form_id' ]}]" );
          @endphp
        </div>
      </div>
    @endif
  </section>
@endif
