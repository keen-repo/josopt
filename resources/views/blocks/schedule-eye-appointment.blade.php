{{--
  Title: Schedule Eye Appointment
  Description: Schedule Eye Appointment
  Category: dg_block_category
  Icon: admin-comments
  Keywords: schedule, appointment
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$vLay = $flds[ 'vertical_layout' ]; // this section takes a vertical/horizontal layout ( bool )
$other_classes = 'layout-horizontal';
if ( $vLay ) {
  $other_classes = 'layout-vertical';
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle
];

$title = $flds[ 'title' ];
$text = $flds[ 'text' ];
$btn = $flds[ 'button' ];
$btnLink = $btn[ 'url' ];
$btnTitle = $btn[ 'title' ];
$btnTarget = $btn[ 'target' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="title-text">
    <div class="title">{{ $title }}</div>
    <div class="text">{{ $text }}</div>
  </div>
  @include (
    'partials.buttons.blue-btn',
    [
      'btnLink'    => $btnLink,
      'btnTitle'   => 'book now',
      'btnTarget'  => $btnTarget,
      'btnType'    => 'outline'
    ]
  )
@endcomponent
