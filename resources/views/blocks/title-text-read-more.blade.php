{{--
  Title: Title, Text, Read more
  Description: Title, Text, Read more
  Category: dg_block_category
  Icon: admin-comments
  Keywords: title, text, read more, abnormal, ocular, condition
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$image         = $flds[ 'image' ];
$image         = aq_resize( $image, 1920, 777, true, true, true );

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image
];

$conditions    = $flds[ 'conditions' ];
$firstColArr   = [  ];
$secColArr     = [  ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="background-blur"></div>
  <div class="conditions">
    <div class="conditions-wrapper">
      @foreach ( $conditions as $condition )
        @php
        $title       = $condition[ 'title' ];
        $text        = $condition[ 'text' ];
        $textShort   = mb_substr( $text, 0, 323 );
        $textShort   = wpautop( $textShort );
        $i           = $loop->index;
        if ( $i % 2 == 0 ) {
          $firstColArr[]   = [ 'title' => $title, 'text' => $text, 'short_text' => $textShort ];
        } else {
          $secColArr[]   = [ 'title' => $title, 'text' => $text, 'short_text' => $textShort ];
        }
        @endphp
      @endforeach
      @php
      $condArr = array_merge( $firstColArr, $secColArr );
      @endphp
      <div class="conditions-col-1">
        @foreach ( $firstColArr as $condition )
          @php
          $title       = $condition[ 'title' ];
          $text        = $condition[ 'text' ];
          $textShort   = mb_substr( $text, 0, 323 );
          $textShort   = wpautop( $textShort );
          @endphp
          <div class="condition">
            <div class="title">{{ $title }}</div>
            <div class="sep"></div>
            <div class="text-content">
              <div class="text">{!! $textShort !!}</div>
              <div class="text-full">{!! $text !!}</div>
            </div>
            @include (
              'partials.buttons.blue-btn',
              [
                'btnLink' => '#',
                'btnTitle' => 'read more',
                'btnType' => 'trans'
              ]
            )
            <span class="dashicons dashicons-arrow-up-alt2"></span>
          </div> <!-- condition -->
        @endforeach
      </div> <!-- conditions-col-1 -->
      <div class="conditions-col-2">
        @foreach ( $secColArr as $condition )
          @php
          $title       = $condition[ 'title' ];
          $text        = $condition[ 'text' ];
          $textShort   = mb_substr( $text, 0, 323 );
          $textShort   = wpautop( $textShort );
          @endphp
          <div class="condition">
            <div class="title">{{ $title }}</div>
            <div class="sep"></div>
            <div class="text-content">
              <div class="text">{!! $textShort !!}</div>
              <div class="text-full">{!! $text !!}</div>
            </div>
            @include (
              'partials.buttons.blue-btn',
              [
                'btnLink' => '#',
                'btnTitle' => 'read more',
                'btnType' => 'trans'
              ]
            )
            <span class="dashicons dashicons-arrow-up-alt2"></span>
          </div> <!-- condition -->
        @endforeach
      </div> <!-- conditions-col-1 -->
    </div> <!-- conditions-wrapper -->
  </div>
@endcomponent
