{{--
  Title: Products Offered
  Description: What we offer
  Category: dg_block_category
  Icon: admin-comments
  Keywords: what, we, offer, products
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image
];

$cs = $flds[ 'product_types' ]
@endphp

@component( 'components.blocks', $componentVars )
  <div class="product-types">
    @foreach ( $cs as $c )
      @php
      $image      = $c[ 'image' ][ 'url' ];
      $img        = aq_resize( $image, 100, 100, false );
      $alt        = $c[ 'image' ][ 'alt' ];
      $title      = $c[ 'title' ];
      $text       = $c[ 'text' ];
      $btn        = $c[ 'button' ];
      if ( $btn ) {
        $btnTitle      = $btn[ 'title' ];
        $btnLink       = $btn[ 'url' ];
        $btnTarget     = $btn[ 'target' ];
      }
      @endphp
      <div class="product-type">
        <div class="image">
          <img src="{{ $img }}" alt="{{ $alt }}">
        </div>
        @if ( $title )
          <div class="title">{{ $title }}</div>
          <div class="sep"></div>
        @endif
        @if ( $text )
          <div class="text">{{ substr( $text, 0, 100 ) }}</div>
        @endif
        @if ( $btn )
          @include ( 'partials/buttons/blue-btn', [ 'btnType' => 'trans' ] )
        @endif
      </div>
    @endforeach
  </div>
@endcomponent
