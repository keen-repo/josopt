{{--
  Title: Eye Exam Guide
  Description: Eye examination guide
  Category: dg_block_category
  Icon: admin-comments
  Keywords: eye, exam, guide
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image
];


$topText = $flds[ 'top_text' ];
$examList = $flds[ 'examination_list' ];
$images = $flds[ 'images' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="top-text">{{ $topText }}</div>
  <div class="examination-wrap">
    <div class="examination-guide">
      @foreach ( $examList as $exam )
        @php
          $title = $exam[ 'title' ];
          $text = $exam[ 'text' ];
        @endphp
        <div class="exam-point">
          <div class="point-title">
            <table>
              <tr>
                <td><span class="number">{{ $loop->iteration }}.</span></td>
                <td><span class="title">{{ $title }}</span></td>
              </tr>
            </table>
          </div>
          <div class="text">{{ $text }}</div>
          @if ( ! $loop->last )
            <div class="sep"></div>
          @endif
        </div>
      @endforeach
    </div>
    <div class="examination-images">
      @foreach ( $images as $image )
        @php
          $img = $image[ 'image' ];
          $img = aq_resize( $img, 738, 523, true, true, true );
        @endphp
        <div class="image"><img src="{{ $img }}" alt=""></div>
      @endforeach
    </div>
  </div>
@endcomponent
