{{--
  Title: Products Collections by Brand
  Description: Products collection image
  Category: dg_block_category
  Icon: admin-comments
  Keywords: product, collection, images, category, categories
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image
];

$cs = $flds[ 'brands' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="images">
    @foreach ( $cs as $c )
      @php
      $catID = $c[ 'select_brand' ];
      if ( $catID ) {
        $btnLink = get_term_link( $catID, 'collection' );
      } else {
        $btnLink = '';
      }

      $image = $c[ 'image' ];
      $img = aq_resize( $image, 382, 461, true, true, true );
      $logo = $c[ 'logo' ];
      $logo = aq_resize( $logo, 200, 100, false, true, true );
      $btnTitle = 'view collection';
      @endphp
      <div class="collection-wrapper">
        <div class="image">
          <img src="{{ $img }}" alt="">
        </div>
        @if ( $btnLink )
        <a href="{{ $btnLink }}" class="overlay"></a>
        @endif
        @include ( 'partials/buttons/blue-btn' )
        <a href="{{ $btnLink }}">
          <div class="logo">
            <img src="{{ $logo }}" alt="">
          </div>
        </a>
      </div>
    @endforeach
  </div>
@endcomponent
