{{--
  Title: Display Team Block
  Description: Display Team Block
  Category: dg_block_category
  Icon: admin-comments
  Keywords: team, our, members
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields( 'options' )[ 'team_members' ];
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$hide_members = get_field( 'hide_team_members' );
@endphp

@include ( 'partials.bobs.display-team-block', [ 'hide_members' => $hide_members ] )
