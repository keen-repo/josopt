{{--
  Title: Home Visits
  Description: Home visits
  Category: dg_block_category
  Icon: admin-comments
  Keywords: home, visits, list, title, bullet
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image
];

$topText  = $flds[ 'top_text' ];
$image    = $flds[ 'image' ];
$img      = $flds[ 'image' ][ 'url' ];
$img      = aq_resize( $img, 241, 9999, false );
$imgTitle = $image[ 'title' ];
$imgAlt   = $image[ 'alt' ];
$lists    = $flds[ 'lists' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="top-text">{{ $topText }}</div>
  <div class="image"><img src="{{ $img }}" alt="{{ $imgAlt }}" title="{{ $imgTitle }}"></div>
  <div class="point-lists">
    @foreach ( $lists as $list )
      @php
      $title = $list[ 'title' ];
      $pointList = $list[ 'list' ];
      @endphp
      <div class="point-list">
        <div class="point-title">{{ $title }}</div>
        <ul>
          @foreach ( $pointList as $points )
            @php
            $point = $points[ 'points' ];
            @endphp
            <li class="point-text">{{ $point }}</li>
          @endforeach
        </ul>
      </div>
    @endforeach
  </div>
@endcomponent
