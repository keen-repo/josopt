{{--
  Title: Insert Maps
  Description: Insert Maps
  Category: dg_block_category
  Icon: admin-comments
  Keywords: map
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds    = get_fields( 'options' )[ 'contact_numbers' ];
$locs    = $flds[ 'locations' ];
$mapID   = 1;

$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  //return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image,
  'no_dots'         => ''
];
@endphp

@component( 'components.blocks', $componentVars )
  <div id="the-maps">
    @foreach ($locs as $loc)
      <div class="map-data" data-lat="{{ $loc['latitude'] }}" data-lng="{{ $loc['longitude'] }}" data-location="{{ strtolower($loc['location']) }}" ></div>
    @endforeach
  </div>

  @php
    $lat        = $locs[0][ 'latitude' ];
    $lng        = $locs[0][ 'longitude' ];
    $location   = $locs[0][ 'location' ];
    $location   = strtolower( $location );
  @endphp

  <div id="{{ $location }}" class="contact-map contact-map-{{ $mapID }}">
    <div id="mapid_{{ $mapID }}" style="height:400px;"></div>
  </div>

  <script>
    jQuery( document ).ready(function($){
      var map_{{ $mapID }} = L.map( 'mapid_{{ $mapID }}' ).setView( [ 35.950789, 14.100695 ], 10 );
      var mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';

      L.tileLayer(
        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: '&copy; ' + mapLink + ' Contributors',
          //minZoom: 11,
          //maxZoom: 11,
        }).addTo(map_{{ $mapID }});

        var agIcon = L.icon({
          iconUrl: "@asset( 'images/icons/map-marker.png' )",
          iconSize: [117,63],
          iconAnchor: [54,63]
        });

      jQuery('#the-maps .map-data').each(function(){

        var lat = jQuery(this).data('lat');
        var lng = jQuery(this).data('lng');

        $( '.get-in-touch .btn' ).on( 'click', function(  ) {
          let $id = $( this ).data( 'id' );
          // $( '.contact-map' ).css( 'display', 'none' );
          $( '#' + $id ).css( 'display', 'block' )
          setTimeout(function() {
            map_{{ $mapID }}.invalidateSize(false);
          }, 400);
        } );

        marker = new L.marker( [ lat, lng ], {icon: agIcon} ).addTo( map_{{ $mapID }} );
      });
    });
  </script>

  
@endcomponent
