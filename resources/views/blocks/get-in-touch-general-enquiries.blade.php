{{--
  Title: Get in touch / General enquiries
  Description: Get in touch / General enquiries
  Category: dg_block_category
  Icon: admin-comments
  Keywords: contact, form
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds = get_fields(  );
$opts = get_fields( 'options' );

$secProp = $flds[ 'sec_prop' ];
$secProp2 = $flds[ 'sec_prop_2' ];

$activeSectionOne = $secProp[ 'active' ];
$activeSectionTwo = $secProp2[ 'active' ];

$titleSectionOne = $secProp[ 'section_title' ];
$titleSectionTwo = $secProp2[ 'section_title' ];

$topText = $flds[ 'top_text' ];
$whoWeAreText = $flds[ 'who_we_are_text' ];

if ( $activeSectionTwo ) {
  $img = aq_resize( $whoImage, 960, 865, true );
} else {
  $img = aq_resize( $whoImage, 1920, 865, true );
}

$btn        = $flds[ 'button' ];
if ( $btn ) {
  $btnTitle      = $btn[ 'title' ];
  $btnLink       = $btn[ 'url' ];
  $btnTarget     = $btn[ 'target' ];
}
@endphp

@if ( $activeSectionOne || $activeSectionTwo )
  <section id="{{ $block[ 'id' ] }}" class="keen-block {{ $block[ 'classes' ] }}">
    @if ( $activeSectionOne )
      <div class="{{ $block[ 'classes' ] }}-wrapper get-in-touch{{ $activeSectionTwo ? '' : ' full-width' }}">
        <div class="full-width-wrapper">
          <h2 class="section-title">{{ $titleSectionOne }}</h2>
          @include ( 'partials.bobs.blue-dots' )
          @include ( 'partials/bobs/get-contact-details', [ 'opts' => $opts ] )
        </div>
      </div>
    @endif

    @if ( $activeSectionTwo )
      <div class="{{ $block[ 'classes' ] }}-wrapper general-enquiries{{ $activeSectionOne ? '' : ' full-width' }}">
        <div class="full-width-wrapper">
          <h2 class="section-title">{{ $titleSectionTwo }}</h2>
          @include ( 'partials.bobs.blue-dots' )
          <div class="top-text">{{ $topText }}</div>
          @php
          echo do_shortcode( "[formidable id={$flds[ 'form_id' ]}]" );
          @endphp
        </div>
      </div>
    @endif
  </section>
@endif
