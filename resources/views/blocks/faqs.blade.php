{{--
  Title: FAQs
  Description: Frequently Asked Questions
  Category: dg_block_category
  Icon: admin-comments
  Keywords: faq, faqs, frequently, questions
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image
];

$topText    = $flds[ 'top_text' ];
$questions  = $flds[ 'questions' ];
@endphp

@component( 'components.blocks', $componentVars )
  <div class="top-text">{{ $topText }}</div>
  <div class="faqs-list">
    @foreach ( $questions as $question )
      @php
      $q = $question[ 'question' ];
      $a = $question[ 'answer' ];
      @endphp
      <div class="jos-qa">
        <div class="question">{{ $q }}</div>
        <div class="answer">{!! $a !!}</div>
      </div>
    @endforeach
  </div>
@endcomponent
