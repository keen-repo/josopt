{{--
  Title: Testimonails
  Description: Testimonails
  Category: dg_block_category
  Icon: admin-comments
  Keywords: testimonails
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$title = $secProp[ 'section_title' ];

$image = $flds[ 'image' ];
$image = aq_resize( $image, 1920, 600, true, true, true );
$testimonials = $flds[ 'testimonials' ];
@endphp

<section id="{{ $block[ 'id' ] }}" class="keen-block {{ $block[ 'classes' ] }}">
  <div class="{{ $block[ 'slug' ] }}-wrapper">
    <div class="testimonials-image" style="background-image: url( '{{ $image }}' );">
      <div class="image-blur"></div>
      <h2 class="section-title">{{ $title }}</h2>
      @include ( 'partials.bobs.blue-dots' )
      <div class="testimonials-slides-wrapper">
        <div class="testimonials-slider">
          @foreach ( $testimonials as $testimonial )
            @php
            $text      = $testimonial[ 'testimonial' ];
            $name      = $testimonial[ 'name' ];
            $location  = $testimonial[ 'location' ];
            @endphp
            <div class="testimonial">
              <div class="testimonial-wrapper">
                <div class="quotes"></div>
                <div class="text">
                  <span>{{ $text }}</span>
                  <div class="sep"></div>
                </div>
                <div class="name">{{ $name }}</div>
                <div class="location">{{ $location }}</div>
              </div>
            </div>
          @endforeach
        </div> <!-- testimonials-slides -->
      </div> <!-- testimonials-slides-wrapper -->
    </div> <!-- testimonials-image -->
  </div>
</section>
