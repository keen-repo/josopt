{{--
  Title: Text Only
  Description: Text only
  Category: dg_block_category
  Icon: admin-comments
  Keywords: text, paragraph
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image,
  'no_dots'         => ''
];

$text = $flds[ 'text' ]
@endphp

@component( 'components.blocks', $componentVars )
  <div class="text">{{ $text }}</div>
@endcomponent
