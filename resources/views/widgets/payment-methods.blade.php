@php
//$opts   = get_fields( 'options' );
$pM     = $opts[ 'payment_methods' ];
$title  = $pM[ 'title' ];
$icons  = $pM[ 'payment_methods' ];
@endphp

<div class="payment-credits">
  <div class="pc-wrapper">
    <div class="payment-methods">
      <div class="wrapper">
        <div class="title">{{ $title }}</div>
        <div class="images">
          @foreach ( $icons as $icon )
            @php
            $iconURL = $icon[ 'icon' ][ 'url' ];
            $img = aq_resize( $iconURL, 9999, 18, false, true, true );
            @endphp
            <div class="image{{ $loop->last ? ' last-item' : '' }}">
              <img src="{{ $img }}">
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
