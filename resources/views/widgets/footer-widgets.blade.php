<section class="widget footer_address">
  <h3 class="widget-title">
    {!! App::getCustomLogo() !!}
  </h3>

  <div class="widget-body">
    @include ( 'partials/contact-details', [ 'opts' => $opts ] )
  </div>
</section>
@php dynamic_sidebar('sidebar-footer') @endphp
