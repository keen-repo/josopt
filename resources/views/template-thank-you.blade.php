{{--
  Template Name: Thank You page
--}}

@extends('layouts.thank-you')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
  @endwhile
@endsection
