@php
global $paged;
if ( ! isset( $paged ) || ! $paged ) {
  $paged = 1;
}

$postNumber = 1;
@endphp

@extends('layouts.app')

@section('content')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts()) @php the_post() @endphp
    @if ( $postNumber == 2 )
    <div class="articles-wrap">
      <div class="articles-wrap-width">
    @endif
      @include('partials.content-'.get_post_type(), [ 'postNum' => 'post-num-' . $postNumber ])
    @if ( $postNumber == 5 )
      </div> <!-- .articles-width-wrap -->
    </div> <!-- .articles-wrap -->
    @endif
    @php
    $postNumber++;
    @endphp
  @endwhile

  @include ( 'partials.posts-pagination' )
@endsection
