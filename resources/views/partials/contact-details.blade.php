@php
//$opts    = get_fields( 'options' );
$flds    = $opts[ 'contact_numbers' ]; // get contact details
$icons   = $opts[ 'social_icons' ][ 'social_icons' ]; // get social icons
$locs    = $flds[ 'locations' ];

$schemaSocialIcons = '';
@endphp

@foreach ( $icons as $icon )
  @php
  $title = $icon[ 'title' ];
  $link = $icon[ 'link' ];
  $icon = $icon[ 'icon' ];
  $schemaSocialIcons .= "\"{$link}\",";
  @endphp
@endforeach

@php
$schemaSocialIcons = rtrim( $schemaSocialIcons, ',' );
@endphp

@foreach ( $locs as $loc )
  @php
  $schemaNumbers = '';
  $schemaEmails = '';

  $location   = $loc[ 'location' ];
  $street     = $loc[ 'street' ];
  $locality   = $loc[ 'locality' ];
  $post_code  = $loc[ 'post_code' ];
  $latitude   = $loc[ 'latitude' ];
  $longitude  = $loc[ 'longitude' ];
  $contacts   = $loc[ 'contacts' ];
  $hours      = $loc[ 'opening_hours' ];
  @endphp
  <div class="location-address{{ $loop->last ? ' last-item' : '' }}">
    <div class="location">{{ $location }}</div>
    <address>{{ $street }}, {{ $locality }} - {{ $post_code }}</address>
    <div class="contact-details">
      @foreach ( $contacts as $contact )
        @php
          $typeVal      = $contact[ 'type' ][ 'value' ];
          $typeLab      = $contact[ 'type' ][ 'label' ];
          $cc           = $contact[ 'country_code' ];
          $num          = $contact[ 'number' ];
          $numNoSpace   = preg_replace( '/[^0-9]/', '', $num );
          $email        = $contact[ 'email' ];
        @endphp
        <div class="contact-detail-wrap">
          @if ( $typeVal != 'email' )
            @php
              $schemaNumber = $cc . $numNoSpace;
              $schemaNumbers .= "\"+{$schemaNumber}\",";
            @endphp
            <span class="title">{{ $typeLab }}:</span><span class="contact-detail"><a href="tel:+{{ $cc }}{{ $numNoSpace }}">{{ $num }}</a></span>
          @else
            @php
              $schemaEmails .= "\"{$email}\",";
            @endphp
            <span class="title">{{ $typeLab }}:</span><span class="contact-detail"><a href="mailto:{{ $email }}">{{ $email }}</a></span>
          @endif
          @if ( ! $loop->last )
            <span class="footer-sep">|</span>
          @endif
        </div>
      @endforeach
      <div class="opening-hours-wrapper">
        <div class="opening-hours-title"><strong>Opening Hours:</strong></div>
        @each( 'components.opening-hours', $hours, 'opening' )
      </div>
      @php
        $schemaNumbers  = rtrim( $schemaNumbers, ',' );
        $schemaEmails   = rtrim( $schemaEmails, ',' );
      @endphp
    </div>
  </div>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "Optician",
    "name": "Joseph Dispensing Opticians",
    "image": "https://woocommerce-209154-1070212.cloudwaysapps.com/wp-content/uploads/2019/12/dgoldline-main-logo.png",
    "@id": "https://josoptician.test",
    "url": "https://josoptician.test",
    "telephone": [ {!! $schemaNumbers !!} ],
    "email": [ {!! $schemaEmails !!} ],
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "{{ $street }}",
      "addressLocality": "{{ $locality }}",
      "postalCode": "{{ $post_code }}",
      "addressCountry": "MT"
    },
    "geo": {
      "@type": "GeoCoordinates",
      "latitude": {{ $latitude }},
      "longitude": {{ $longitude }}
    } ,
    "sameAs": [ {!! $schemaSocialIcons !!} ]
  }
  </script>
@endforeach
