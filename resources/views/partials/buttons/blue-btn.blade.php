@if ( $btnLink )
  <div class="btn-wrap">
    <a href="{{ $btnLink }}" class="btn btn-{{ $btnType ? $btnType : 'blue' }} {{ $classes }}"@if ( $btnID ) data-id="{{ $btnID }}" @endif>{{ $btnTitle ? $btnTitle : 'learn more' }}</a>
  </div>
@endif
