@include( 'partials.preloader' )
<div class="eu-strip">
  <a href="https://businessenhance.gov.mt/" target="_blank"><img src="@asset( 'images/eu-logo.jpg' )" alt="" /></a>
</div>
<header class="banner{{ is_checkout(  ) ? ' checkout-banner' : '' }}">
  @if ( is_checkout(  ) )
    <a href="{{ get_home_url( ) }}" class="custom-logo-link" rel="home">
      <img width="255" height="75" src="@asset( 'images/jos-logo-dark.png' )" class="custom-logo" alt="Joseph Dispensing Opticians">
    </a>
    <div class="checkout-steps-banner">
      <div class="step-one">
        <div class="step-number">STEP 1</div>
        <div class="step-title">ENTER YOUR PERSONAL DETAILS</div>
        @include ( 'partials.bobs.blue-dots' )
      </div>
      <div class="step-two">
        <div class="step-number">STEP 2</div>
        <div class="step-title">CONFIRM YOUR ORDER</div>
        @include ( 'partials.bobs.blue-dots' )
        <span class="step-back">< back</span>
      </div>
    </div>
  @endif
  @if ( ! is_checkout(  ) )
  <div class="grad-v"></div>
  @endif
  <div class="container">
    @if ( ! is_front_page() )
      <a class="site-brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
    @endif
    <div class="main-menu-open"><span class="dashicons dashicons-menu-alt3"></span></div>
    @if ( ! is_checkout(  ) )
      {!! App::getCustomLogo() !!}
    @endif
    @if ( ! is_checkout(  ) )
    @include( 'partials/menus.main-menu' )
    @include( 'partials/menus.login-register-menu' )
    @endif
  </div>
  @if ( is_front_page() )
    @include ( 'partials.hero-image' )
  @endif
</header>
