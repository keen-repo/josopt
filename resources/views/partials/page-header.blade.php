<div class="page-header">
  @php
  if ( is_shop(  ) ) {
    $postID = get_option( 'woocommerce_shop_page_id' );
  } elseif ( is_home() ) {
    $blogPageID = get_option( 'page_for_posts' );
    $postID = $blogPageID;
  } elseif ( is_single() ) {
    $blogPageID = get_option( 'page_for_posts' );
    $postID = $blogPageID;
  } else {
    $postID = get_the_ID();
  }

  if ( is_product_category(  ) ) {
    $obj            = get_queried_object();
    $term_slug      = $obj->slug;
    $taxonomy       = $obj->taxonomy;
    $term_id        = get_term_by( 'slug', $term_slug, $taxonomy )->term_id;
    $thumbnail_id   = get_woocommerce_term_meta( $term_id, 'thumbnail_id', true );
    $image          = wp_get_attachment_url( $thumbnail_id );
    $featImg        = $image;
  } else {
    $featImg = get_the_post_thumbnail_url( $postID, 'full' );
  }

  $featImg = aq_resize( $featImg, 1920, 385, true, true, true );
  @endphp

  @if ( ! is_checkout(  ) )
    <div class="post-thumbnail" style="background-image: url( '{{ $featImg }}' );">
      <div class="grad-h"></div>
      {{--<img src="{{ $featImg }}" alt="">--}}
      @if ( is_shop(  ) )
        <h1>contact lenses</h1>
      @elseif ( is_single(  ) && ! is_product(  ) )
        <h1>News</h1>
      @elseif ( is_product(  ) )
        @php
        global $product;
        $prodID = $product->get_id(  );
        $prodCats = $product->get_categories(  );
        $cat = wp_get_post_terms( $prodID, 'product_cat' )
        @endphp
        <h1>{!! $cat[ 0 ]->name !!}</h1>
      @else
        <h1>{!! App::title() !!}</h1>
      @endif
    </div>
  @endif
</div>
