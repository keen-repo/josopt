@php
$opts = get_fields( 'options' );
$titleBold      = 'Join Our';
$titleRegular   = 'Newsletter';
@endphp

<div class="dg-wc-login-form">
  @php
  woocommerce_login_form(  );
  @endphp
</div>

<footer class="content-info">
  @if ( ! is_404() )
  {{--@include ( 'widgets/newsletter' )--}}
  @endif
  <div class="container">
    <div class="widgets">
      @include ( 'widgets.footer-widgets', [ 'opts' => $opts ] )
    </div>
    <div class="eu-logo-container">
      <a href="https://eufunds.gov.mt/en/Pages/Home.aspx" target="_blank"><img class="eu-logo" src="@asset('images/eu-footer.jpg')" alt=""></a>
    </div>
    <div class="payments-social-icons-credits">
      <div class="social-icons-credits">
        @include( 'partials.social-icons' )
        @include( 'partials.terms-privacy' )
        @include ( 'widgets.payment-methods', [ 'opts' => $opts ] )
        @include( 'partials.footer-credits' )
      </div>
    </div>
  </div>
</footer>
{{--
<div class="search-form-wrapper">
  <div class="wrapper">{{ get_search_form(  ) }}</div>
  <div class="btn-close">Close</div>
</div>
--}}
