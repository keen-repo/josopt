<nav class="login-register-menu">
  <ul class="login-navigation list-unstyled">
    <li class="loginout-menu-item">
      <a href="{!! App::login_register_menu()[ 'account' ] !!}">
        <img src="@asset( 'images/user.png' )" alt="">
      </a>
    </li>
    <li class="dg-cart-basket-icon">
      <a class="last-item" href="{{ App::login_register_menu()[ 'cart_url' ] }}">
        <img src="@asset( 'images/shopping-cart.png' )">
        <span class="cart-count">@php echo WC()->cart->get_cart_contents_count(); @endphp</span>
      </a>
      @include( 'partials.shop.cart-popup' )
    </li>
  </ul>
</nav>
