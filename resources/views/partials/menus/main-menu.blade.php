<nav id="site-navigation" class="main-navigation nav-primary">
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav list-unstyled']) !!}
  @endif
  <button class="btn btn-close">close</button>
</nav>
