@php
$flds  = get_fields( 'options' )[ 'team_members' ];
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => 'team-members',
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle
];

$top_text      = wpautop( $flds[ 'top_text' ] );
$job_types     = $flds[ 'job_types' ];
$team_members  = $flds[ 'team_members' ];
$button        = $flds[ 'button' ];
if ( $button ) {
  $btnLink = $button[ 'url' ];
  $btnTitle = $button[ 'title' ];
  $btnTarget = $button[ 'target' ];
}
if ( $flds[ 'hide_team_members' ] ) {
  $hide_members  = $flds[ 'hide_team_members' ];
}
@endphp

<section id="{{ $block[ 'id' ] }}" class="keen-block team-members">
  <div class="{{ $block[ 'classes' ] }}-wrapper">
    <div class="section-top">
      <h2 class="section-title">{{ $sectionTitle }}</h2>
      @include ( 'partials.bobs.blue-dots' )
      <div class="top-text">{!! $top_text !!}</div>
      <div class="job-types">
        @foreach ( $job_types as $job_type )
          @php
          $type = $job_type[ 'type' ];
          $text = $job_type[ 'text' ];
          @endphp
          <div class="job-type-wrap{{ $loop->last ? ' last-item' : '' }}">
            <div class="job-type">
              <span>{{ $type }}</span>
              <div class="sep"></div>
            </div>
            <div class="job-text">{{ $text }}</div>
          </div>
        @endforeach
      </div>
    </div> <!-- top-section -->
    @if ( ! $hide_members )
      <div class="section-bottom">
        <div class="team-members-wrap">
          <div class="team-members-wrapper">
            @foreach ( $team_members as $team_member )
              @php
              $image     = $team_member[ 'image' ];
              $image     = aq_resize( $image, 214, 214, true, true, true );
              $name      = $team_member[ 'name' ];
              $job_type  = $team_member[ 'job_type' ];
              $location  = $team_member[ 'location' ];
              @endphp
              <div class="team-member">
                <div class="image"><img src="{{ $image }}" alt="{{ $name }} - {{ $job_type }} in {{ $location }}"></div>
                <div class="name">
                  <span>{{ $name }}</span>
                  <div class="sep"></div>
                </div>
                <div class="job-type">{{ $job_type }}</div>
                <div class="location">{{ $location }}</div>
              </div>
            @endforeach
          </div> <!-- team-members-wrapper -->
        </div> <!-- team-member-wrap -->
        @include (
          'partials.buttons.blue-btn',
          [
            'btnLink' => $btnLink,
            'btnTitle' => $btnTitle,
            'btnTarget' => $btnTarget
          ]
        )
      </div> <!-- bottom-section -->
    @endif
  </div>
</section>
