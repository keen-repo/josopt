@php
//$opts    = get_fields( 'options' );
$flds    = $opts[ 'contact_numbers' ]; // get contact details
$locs    = $flds[ 'locations' ];
$mapID   = 1;

// Emergency Contact Number
$emNum          = $flds[ 'emergency_contact_number' ];
$emCountryCode  = $emNum[ 'country_code' ];
$emNumber       = $emNum[ 'number' ];
$emNumNoSpace   = preg_replace( '/[^0-9]/', '', $emNumber );
@endphp

@foreach ( $locs as $loc )
  @php
  $location   = $loc[ 'location' ];
  $locLower   = strtolower( $location );
  $street     = $loc[ 'street' ];
  $locality   = $loc[ 'locality' ];
  $post_code  = $loc[ 'post_code' ];
  $lat        = $loc[ 'latitude' ];
  $lng        = $loc[ 'longitude' ];
  $contacts   = $loc[ 'contacts' ];
  $typePhone = 0;
  $typeEmail = 0;
  @endphp
  <div class="location-address{{ $loop->last ? ' last-item' : '' }}">
    <div class="location">{{ $location }}</div>
    <div class="address">
      <address class="contact-detail">{{ $street }}, {{ $locality }} - {{ $post_code }}</address>
    </div>
    <div class="contact-details">
      @foreach ( $contacts as $contact )
        @php
          $typeVal      = $contact[ 'type' ][ 'value' ];
          $typeLab      = $contact[ 'type' ][ 'label' ];
          $cc           = $contact[ 'country_code' ];
          $num          = $contact[ 'number' ];
          $numNoSpace   = preg_replace( '/[^0-9]/', '', $num );
          $email        = $contact[ 'email' ];
        @endphp
        @if ( $typeVal != 'email' )
          <div class="contact-detail-wrap{{ $typePhone == 0 ? ' phone' : '' }}">
            <span class="contact-detail"><a href="tel:+{{ $cc }}{{ $numNoSpace }}">+{{ $cc }} {{ $num }}</a></span>
          </div>
          @php
          $typePhone++;
          @endphp
        @else
          <div class="contact-detail-wrap{{ $typeEmail == 0 ? ' email' : '' }}">
            <span class="contact-detail"><a href="mailto:{{ $email }}">{{ $email }}</a></span>
          </div>
          @php
          $typeEmail++;
          @endphp
        @endif
      @endforeach
      @include (
        'partials.buttons.blue-btn',
        [
          'btnLink'   => '#the-maps',
          'btnTitle'  => 'view on map',
          'btnTarget' => '',
          'btnType'   => 'trans',
          'btnID'     => $locLower
        ]
      )
      <div class="border-bottom"></div>
    </div>
    @if ( $lat and $lngs )
      <div class="contact-map">
        <div id="mapid_{{ $mapID }}" style="height:400px;"></div>
      </div>
      <script>
        jQuery( document ).ready( function(  ) {
          var map_{{ $mapID }} = L.map( 'mapid_{{ $mapID }}' ).setView( [ {{ $lat }}, {{ $lng }} ], 16 );
          var mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';

          L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution: '&copy; ' + mapLink + ' Contributors',
              //minZoom: 11,
              //maxZoom: 11,
            }).addTo(map_{{ $mapID }});

          var agIcon = L.icon({
            iconUrl: "@asset( 'images/icons/map-marker.png' )",
            iconSize: [117,63],
            iconAnchor: [54,63]
          });

          marker = new L.marker( [ {{ $lat }}, {{ $lng }} ], {icon: agIcon} ).addTo( map_{{ $mapID }} );
        } );
      </script>
    @endif
  </div>
  @php
  $mapID++;
  @endphp
@endforeach
{{-- <div class="emergency-details">
  <div class="emergency-text">For Emergency Use Only:</div>
  <div class="emergency-number"><a href="tel:+{{ $emCountryCode }}{{ $emNumNoSpace }}">+{{ $emCountryCode }} {{ $emNumber }}</a></div>
</div> --}}
