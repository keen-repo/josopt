@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];

$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionTitle = $secProp[ 'section_title' ];
$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => ' products-slider',
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'backImg'         => $image
];

$cs = $flds[ 'select_products' ];

$prodIDArray = [  ];

if ( empty( $cs ) ) {
  return;
}
@endphp

@component( 'components.blocks', $componentVars )
  <div class="products-slides">
    @foreach ( $cs as $c )
      @php
      $prodID = $c->ID;
      array_push( $prodIDArray, $prodID );
      @endphp
    @endforeach
    @php
      $prodIDString = implode( ', ', $prodIDArray );
      echo do_shortcode( "[products ids='{$prodIDString}' columns=1]" );
    @endphp
  </div>
@endcomponent
