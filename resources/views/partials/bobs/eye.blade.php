@php
$title     = $allFields[ 'title' ];
$text      = $allFields[ 'text' ];
$btn       = $allFields[ 'button' ];
if ( $btn ) {
  $btnTitle    = $btn[ 'title' ];
  $btnLink     = $btn[ 'url' ];
  $btnTarget   = $btn[ 'target' ];
}
@endphp

<div class="jos-eye-wrapper large">
  <div class="jos-eye">
    <img src="@asset( 'images/eye-large.svg' )" alt="">
  </div>
  <div class="jos-eye-content">
    <div class="title">{!! $title !!}</div>
    <div class="text">{!! $text !!}</div>
    @include ( 'partials/buttons/blue-btn', [ 'btnType' => 'outline' ] )
  </div>
</div>

<div class="jos-eye-wrapper small">
  <div class="jos-eye">
    <img src="@asset( 'images/eye-small.svg' )" alt="">
  </div>
  <div class="jos-eye-content">
    <div class="title">{!! $title !!}</div>
    <div class="text">{!! $text !!}</div>
    @include ( 'partials/buttons/blue-btn', [ 'btnType' => 'outline' ] )
  </div>
</div>
