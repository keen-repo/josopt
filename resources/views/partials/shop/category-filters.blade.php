@php
$obj            = get_queried_object();
$term_slug      = $obj->slug;

if ( $term_slug === 'contact-lenses') {
  return;
}

// function getPriceFil(  ) {
//   global $wpdb;

//   $args       = WC()->query->get_main_query()->query_vars;
//   $tax_query  = isset( $args['tax_query'] ) ? $args['tax_query'] : array();
//   $meta_query = isset( $args['meta_query'] ) ? $args['meta_query'] : array();

//   if ( ! is_post_type_archive( 'product' ) && ! empty( $args['taxonomy'] ) && ! empty( $args['term'] ) ) {
//     $tax_query[] = WC()->query->get_main_tax_query();
//   }

//   foreach ( $meta_query + $tax_query as $key => $query ) {
//     if ( ! empty( $query['price_filter'] ) || ! empty( $query['rating_filter'] ) ) {
//       unset( $meta_query[ $key ] );
//     }
//   }

//   $meta_query = new WP_Meta_Query( $meta_query );
//   $tax_query  = new WP_Tax_Query( $tax_query );
//   $search     = WC_Query::get_main_search_query_sql();

//   $meta_query_sql   = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
//   $tax_query_sql    = $tax_query->get_sql( $wpdb->posts, 'ID' );
//   $search_query_sql = $search ? ' AND ' . $search : '';

//   $sql = "
//     SELECT min( min_price ) as min_price, MAX( max_price ) as max_price
//     FROM {$wpdb->wc_product_meta_lookup}
//     WHERE product_id IN (
//       SELECT ID FROM {$wpdb->posts}
//       " . $tax_query_sql['join'] . $meta_query_sql['join'] . "
//       WHERE {$wpdb->posts}.post_type IN ('" . implode( "','", array_map( 'esc_sql', apply_filters( 'woocommerce_price_filter_post_type', array( 'product' ) ) ) ) . "')
//       AND {$wpdb->posts}.post_status = 'publish'
//       " . $tax_query_sql['where'] . $meta_query_sql['where'] . $search_query_sql . '
//     )';

//   $sql = apply_filters( 'woocommerce_price_filter_sql', $sql, $meta_query_sql, $tax_query_sql );

//   return $wpdb->get_row( $wpdb->prepare($sql) );
// }

// $maxPrice = getPriceFil(  )->max_price;

// if ( $maxPrice >= 20 ) {
//   $priceIterator = 10;
//   $priceIncement = 10;
// } else {
//   $priceIterator = 1;
//   $priceIncement = 1;
// }
@endphp

<div id="jos-category-filters">
  <div class="jos-category-filters">
    <div class="jos-category-filters-wrapper">
      @php
      $subheadingvalues = get_terms(
                            'pa_gender',
                            [
                              'hide_empty' => false,
                            ]
                          );
      @endphp
      <div class="select-wrapper filter_gender-wrap">
        <select class="filter_gender">
          <option data-id="0" value="">gender</option>
          @foreach ($subheadingvalues as $subheadingvalue)
            <option data-search="filter_gender" value="{{ $subheadingvalue->slug }}" {{ isset($_GET["filter_gender"]) ? ($_GET["filter_gender"] == $subheadingvalue->slug ? "selected" : "" ) : "" }}>{{ $subheadingvalue->name }}</option>
          @endforeach
        </select>
      </div>

      @php
      $subheadingvalues = get_terms(
                            'pa_brand',
                            [
                              'hide_empty' => false,
                            ]
                          );
      @endphp
      <div class="select-wrapper filter_brand-wrap">
        <select class="filter_brand">
          <option data-id="0" value="">sort by brand</option>
          @foreach ($subheadingvalues as $subheadingvalue)
            <option data-search="filter_brand" value="{{ $subheadingvalue->slug }}" {{ isset($_GET["filter_brand"]) ? ($_GET["filter_brand"] == $subheadingvalue->slug ? "selected" : "" ) : "" }}>{{ $subheadingvalue->name }}</option>
          @endforeach
        </select>
      </div>

      <div class="select-wrapper max_price-wrap">
        <select class="max_price">
          <option value="">sort by price</option>
          <option data-searchmin="min_price" data-searchminval="10" data-search="minmax1" data-searchmaxval="100" value="100">< &euro;10 - &euro;100</option>
          <option data-searchmin="min_price" data-searchminval="100" data-search="minmax2" data-searchmaxval="500" value="500">< &euro;100 - &euro;500</option>
          <option data-search="minmax3" value="9999">< &euro;201+</option>
        </select>
      </div>

      {{--
      <div class="select-wrapper max_price-wrap">
        <select class="max_price">
          <option value="">sort by price</option>
          @while ( $priceIterator <= $maxPrice )
            <option data-search="max_price" value="{{ $priceIterator }}">< &euro;{{ $priceIterator }}</option>
            @php
            $priceIterator += $priceIncement;
            @endphp
          @endwhile
        </select>
      </div>
      --}}
    </div>

    <div class="reset-filters btn btn-blue">RESET</div>

  </div>
</div>
