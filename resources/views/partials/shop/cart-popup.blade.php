@php
global $woocommerce;
$items = $woocommerce->cart->get_cart();
$checkoutURL = $woocommerce->cart->get_checkout_url();
//$checkoutURL = get_fields( 'options' )[ 'page_links' ][ 'checkout_delivery_options' ];

$prodQtys = $woocommerce->cart->get_cart_item_quantities(  );

$totalQtys = $woocommerce->cart->get_cart_contents_count(  );
if ( $totalQtys == 1 ) {
  $itemText = 'ITEM';
} else {
  $itemText = 'ITEMS';
}

// shop page link
$btnLinkShop = get_permalink( wc_get_page_id( 'shop' ) );
$btnLinkCart = get_permalink( wc_get_page_id( 'cart' ) );
@endphp
<div class="dg-cart-popup">
  <div class="total-quantities">
    <strong>YOUR BASKET</strong>
    <img src="@asset( 'images/icons/close.png' )" class="close">
    {{--<span class="quantities">{{ $totalQtys }} {{ $itemText }}</span>--}}
  </div>
  <div class="dg-cart-products">
    @php
    foreach( $items as $item => $values ) {
      $_product =  wc_get_product( $values[ 'data' ]->get_id() );
      $price = get_post_meta( $values[ 'product_id' ] , '_price', true );
      $currency = get_woocommerce_currency_symbol(  );
      $productDetail = wc_get_product( $values[ 'product_id' ] );
      $productAttr = $_product->get_attributes(  );
      $subTotal = $woocommerce->cart->get_cart_subtotal();
      $productID = $productDetail->get_id(  );
      $_prod = apply_filters( 'woocommerce_cart_item_product', $values['data'], $values, $item );
      $prodDesc = str_limit( $_product->get_description(), 80 );
      @endphp
      <div class="dg-cart-product jos-product-id-{{ $_product->get_id(  ) }}" data-id="{{ $_product->get_id(  ) }}">
        {{-- Image --}}
        <div class="dg-cart-image">
        {!! $productDetail->get_image( 'woocommerce_thumbnail' ) !!}
        </div>
        <div class="dg-cart-content">
          {{-- Title --}}
          <div class="product-title">{{ $_product->get_title() }}</div>
          {{-- Product Quantity --}}
          <div class="product-qty"><span class="title">QTY.</span><span class="text">{{ $values[ 'quantity' ] }}</span></div>

          <div class="price-bin">
            {{-- Price --}}
            <div class="price">{!! $currency !!}{{ $price }}</div>

            {{-- Remove trash can --}}
            @php
            echo apply_filters(
              'woocommerce_cart_item_remove_link',
              sprintf(
                '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&nbsp;</a>',
                esc_url( wc_get_cart_remove_url( $item ) ),
                esc_html__( 'Remove this item', 'woocommerce' ),
                esc_attr( $_prod ),
                esc_attr( $_product->get_sku() )
              ),
              $item
            );
            @endphp
            {{-- Remove trash can - END --}}
          </div> <!-- price / trash can -->
        </div>
        @php
        echo wc_get_formatted_cart_item_data( $values ); // PHPCS: XSS ok.
        @endphp
        {{-- Subtotal --}}
      </div>
      @php
    }
    @endphp
  </div>
  <div class="dg-cart-subtotal">
    <span class="title">Basket Total:</span>
    <span class="subtotal-amount">{!! $subTotal !!}</span>
  </div>
  <div class="dg-cart-buttons">
    <!-- View Cart button -->
    @include ( 'partials.buttons.blue-btn', [
      'btnLink' => $btnLinkCart,
      'btnTitle' => 'view cart',
      'btnType' => 'outline',
      'classes' => 'view-cart'
    ] )
    <!-- Checkout button -->
    @if ( is_user_logged_in() )
      @include ( 'partials.buttons.blue-btn', [
        'btnLink' => $checkoutURL,
        'btnTitle' => 'checkout',
        'classes' => 'checkout-btn'
      ] )
    @else
      @include ( 'partials.buttons.blue-btn', [
        'btnLink' => '#',
        'btnTitle' => 'checkout',
        'classes' => 'checkout-btn logged-out'
      ] )
    @endif
    <!-- Continue Shopping button -->
    <div class="dg-continue-shopping-wrap">
      @include ( 'partials.buttons.blue-btn', [
        'btnLink' => $btnLinkShop,
        'btnTitle' => 'continue shopping',
        'btnType' => 'outline',
        'classes' => 'continue-shopping'
      ] )
    </div>
  </div>
</div>
