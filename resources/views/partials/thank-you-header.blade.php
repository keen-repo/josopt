<div class="pre-loader-wrap"><div class="pre-loader">Loading...</div></div>
<header class="banner checkout-banner">
  <a href="//localhost:3000/" class="custom-logo-link" rel="home">
    <img width="255" height="75" src="@asset( 'images/jos-logo-dark.png' )" class="custom-logo" alt="Joseph Dispensing Opticians">
  </a>
  <div class="checkout-steps-banner">
    <div class="step-one">
      <div class="step-title">ORDER COMPLETE</div>
      @include ( 'partials.bobs.blue-dots' )
    </div>
  </div>
</header>
