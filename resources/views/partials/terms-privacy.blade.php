<div class="credit-page-links">
  <div class="credit terms-conditions"><a href="{{ $opts[ 'page_links' ][ 'terms_conditions' ] }}">Terms &amp; Conditions</a></div>
  <span class="footer-sep">|</span>
  <div class="credit privacy-policy"><a href="{{ $opts[ 'page_links' ][ 'privacy_policy' ] }}">Privacy Policy</a></div>
</div>