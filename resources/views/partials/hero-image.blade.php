<section class="hero-images">
  <div class="grad-h"></div>
  @php
  $allFields  = get_fields(  );
  $flds       = $allFields[ 'hero_images' ];
  @endphp
  <div class="hero-slides">
    @foreach ( $flds as $fld )
      <div class="hero-slider">
        <div class="hero-slide-wrap">
          <div class="slide-wrapper">
            @php
              $image       = $fld[ 'image' ];
              $img         = aq_resize( $image[ 'url' ], 1920, 1080, true );
            @endphp

            <div class="image-wrapper">
              <div class="image" style="background-image: url( '{{ $img }}' )"></div>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
  <div class="hero-slides-content-wrapper">
    <div class="hero-slides-content">
      @foreach ( $flds as $fld )
        <div class="hero-slider">
          <div class="hero-slide-wrap">
            <div class="slide-wrapper">
              @php
                $title       = $fld[ 'title' ];
                $text        = $fld[ 'text' ];
                $btn         = $fld[ 'button' ];
                if ( $btn ) {
                  $btnTitle    = $btn[ 'title' ];
                  $btnLink     = $btn[ 'url' ];
                  $btnTarget   = $btn[ 'target' ];
                }
              @endphp

              <div class="slide-content">
                <div class="content-box">
                  @if ( $title )
                  <h2 class="title">{{ $title }}</h2>
                  @endif
                  @if ( $text )
                  <div class="text">{{ $text }}</div>
                  @endif
                  @include ( 'partials/buttons/blue-btn' )
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  @include ( 'partials/bobs/eye' )
</section>
{{--
<style type="text/css">
  [data-{{$block['id']}}] {
    background: {{get_field('color')}};
  }
</style>
--}}
