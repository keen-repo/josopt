<div class="credits">
  <div class="credit-copy-design">
    <div class="credit copyright">&copy; {{ date( 'Y' ) }} {{ get_bloginfo( 'name', 'display' ) }}. All rights reserved.</div>
    <span class="footer-sep">|</span>
    <div class="credit design"><a href="https://keen.com.mt">Designed and Developed by Keen Malta</a></div>
  </div>
</div>
