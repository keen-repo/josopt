@php
$thumbURL = get_the_post_thumbnail_url(  );
$img = aq_resize( $thumbURL, 624, 457, true, true, true );
$link = get_permalink();
@endphp

<article @php post_class( "{$postNum}" ) @endphp>
  <div class="article-width">
    <div class="article-wrapper">
      <div class="post-thumbnail">
        @if ( has_post_thumbnail(  ) )
          <a href="{{ $link }}" rel="bookmark"><img src="{{ $img }}" alt=""></a>
        @else
          <a href="{{ $link }}" rel="bookmark"><img src="@asset( 'images/rep-img.png' )" alt="Joseph Dispensing Opticians"></a>
        @endif
      </div>
      <div class="text-wrapper">
        <header>
          @include('partials/entry-meta')
          <h2 class="entry-title"><a href="{{ $link }}" rel="bookmark">{!! get_the_title() !!}</a></h2>
        </header>
        <div class="sep"></div>
        <div class="entry-summary">
          @php the_excerpt() @endphp
        </div>
        @include (
          'partials.buttons.blue-btn',
          [
            'btnLink' => $link,
            'btnTitle' => 'read more',
            'btnType' => 'trans'
          ]
        )
      </div> <!-- .text-wrapper -->
    </div> <!-- .article-wrapper -->
  </div> <!-- .article-width -->
</article>
