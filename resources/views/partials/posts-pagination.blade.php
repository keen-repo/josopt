@php
global $wp_query;

$big = 9999999999999;
$base = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );
$format = '?paged=%#%';
$current = get_query_var( 'paged' );
$total = $wp_query->max_num_pages;
@endphp

<div class="posts-pagination">
  <nav class="posts-navigation">
      @php
          $pagedLinks = paginate_links( [
              'base'         => $base,
              'format'       => $format,
              'add_args'     => false,
              'current'      => max( 1, $current ),
              'total'        => $total,
              'prev_text'    => '&larr;',
              'next_text'    => '&rarr;',
              'type'         => 'list',
              'end_size'     => 2,
              'mid_size'     => 0,
          ] );
      @endphp
      {!! $pagedLinks !!}
  </nav>
</div>
