@extends('layouts.page-404')

@php
$btnLink = get_home_url();
$btnTitle = 'go to homepage';
@endphp

@section('content')
  @if (!have_posts())
    <div class="alert alert-warning">
      <div class="error404-image"><img src="@asset( 'images/error_404.png' )" alt=""></div>
      <div class="sep"></div>
      {{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}
      @include (
        'partials.buttons.blue-btn',
        [
          'btnLink'    => $btnLink,
          'btnTitle'   => $btnTitle,
          'btnTarget'  => '',
        ]
      )
    </div>
  @endif
@endsection
