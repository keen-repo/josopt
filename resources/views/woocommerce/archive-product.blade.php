{{--
The Template for displaying product archives, including the main shop page which is a post type archive

This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see https://docs.woocommerce.com/document/template-structure/
@package WooCommerce/Templates
@version 3.4.0
--}}
@php
defined( 'ABSPATH' ) || exit;
@endphp

@extends( 'layouts.shop' )

@section( 'content' )
  @php
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
    do_action('get_header', 'shop');
    do_action('woocommerce_before_main_content');
  @endphp

  @if ( is_product_category(  ) )
    @include( 'partials.shop.category-filters' )
  @endif

  <header class="woocommerce-products-header">
  	@php
    /*
    if ( apply_filters( 'woocommerce_show_page_title', true ) ) : @endphp
  	<h1 class="woocommerce-products-header__title page-title">@php woocommerce_page_title(); @endphp</h1>
  	@php
    endif;
    */
    @endphp

  	@php
  	/**
  	 * Hook: woocommerce_archive_description.
  	 *
  	 * @hooked woocommerce_taxonomy_archive_description - 10
  	 * @hooked woocommerce_product_archive_description - 10
  	 */
  	//do_action( 'woocommerce_archive_description' );
  	@endphp
  </header>

  @php
  if ( woocommerce_product_loop() ) {

  	/**
  	 * Hook: woocommerce_before_shop_loop.
  	 *
  	 * @hooked woocommerce_output_all_notices - 10
  	 * @hooked woocommerce_result_count - 20
  	 * @hooked woocommerce_catalog_ordering - 30
  	 */
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
  	do_action( 'woocommerce_before_shop_loop' );
    @endphp
    <div class="shop-wrapper">
      @php
    	woocommerce_product_loop_start();

    	if ( wc_get_loop_prop( 'total' ) ) {
    		while ( have_posts() ) {
    			the_post();

    			/**
    			 * Hook: woocommerce_shop_loop.
    			 */
    			do_action( 'woocommerce_shop_loop' );

    			wc_get_template_part( 'content', 'product' );
    		}
    	}

    	woocommerce_product_loop_end();
      @endphp
      @include ( 'partials.bobs.blue-dots' )
    </div> <!-- .shop-wrapper -->
    @php

  	/**
  	 * Hook: woocommerce_after_shop_loop.
  	 *
  	 * @hooked woocommerce_pagination - 10
  	 */
  	do_action( 'woocommerce_after_shop_loop' );
  } else {
  	/**
  	 * Hook: woocommerce_no_products_found.
  	 *
  	 * @hooked wc_no_products_found - 10
  	 */
  	do_action( 'woocommerce_no_products_found' );
  }

  /**
   * Hook: woocommerce_after_main_content.
   *
   * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
   */
  do_action( 'woocommerce_after_main_content' );
  @endphp

  @php
  $obj                  = get_queried_object();
  $categoryDesc         = wpautop( $obj->description );
  $categorySlug         = $obj->slug;
  $opts                 = get_fields( 'options' );
  $contactLink          = $opts[ 'page_links' ][ 'contact_page' ];
  $brands               = $opts[ 'brands' ][ 'brands' ];
  @endphp

  @if ( is_product_category(  ) && $categorySlug !== 'contact-lenses' )
    <div class="enquire-now-wrapper">
      @include(
        'partials.buttons.blue-btn',
        [
          'btnLink'   => $contactLink,
          'btnTitle'  => 'enquire now'
        ]
      )
    </div>
  @endif

  @if ( $categoryDesc )
    <div class="shop-page-bottom-text">
      <div class="shop-page-bottom-text-wrapper">
        {!! $categoryDesc !!}
      </div>
    </div>
  @endif

  @if ( is_product_category(  ) && $categorySlug !== 'contact-lenses' )
    @if ( $brands )
      <div class="brands-wrapper">
        <h2 class="section-title">Our brands</h2>
        @include ( 'partials.bobs.blue-dots' )
        <div class="brands-width">
          @foreach ( $brands as $brand )
            @php
            $title = $brand[ 'name' ];
            $image = $brand[ 'image' ][ 'url' ];
            $img   = aq_resize( $image, 160, 72, false, true, true );
            $alt   = $brand[ 'image' ][ 'alt' ];
            @endphp
            <div class="brand">
              <img src="{{ $img }}" alt="{{ $alt }}" title="{{ $title }}">
            </div>
          @endforeach
        </div>
      </div>
    @endif
  @endif

  @include ( 'partials.bobs.schedule-eye-examination' )

  @php
  /**
   * Hook: woocommerce_sidebar.
   *
   * @hooked woocommerce_get_sidebar - 10
   */
  //do_action( 'woocommerce_sidebar' );

  do_action( 'get_footer', 'shop' );
  @endphp
@endsection
