<section id="{{ $id }}" class="keen-block {{ $classes }}{{ $other_classes }}"@if ( $backImg ) style="background-image:url( '{{ $backImg }}' );" @endif>
  <div class="{{ $slug }}-wrapper">
    @if ( $title )
      <h2 class="section-title">{{ $title }}</h2>
      @if ( ! $no_dots )
        @include ( 'partials.bobs.blue-dots' )
      @endif
    @endif
    {{ $slot }}
  </div>
</section>
