@php
$days     = $opening["days"];
$amOpen   = $opening["morning"]["opening_time"];
$amClose  = $opening["morning"]["closing_time"];
$pmOpen   = $opening["afternoon"]["opening_time"];
$pmClose  = $opening["afternoon"]["closing_time"];
@endphp

<span class="day">{{ $days }} - {{ $amOpen }} - {{ $amClose }} @if ( $pmOpen and $pmClose ) | {{ $pmOpen }} - {{ $pmClose }} @endif</span><br>

{{--
<div class="opening-date">
  <div class="day">{{ $days }}</div>
  <div class="hours">
    <div class="meridian morning">
      <div class="morning-open">{{ $amOpen }}</div>
      <div class="morning-close">{{ $amClose }}</div>
    </div>
    @if ( $pmOpen and $pmClose )
      <div class="meridian afternoon">
        <div class="afternoon-open">{{ $pmOpen }}</div>
        <div class="afternoon-close">{{ $pmClose }}</div>
      </div>
    @endif
  </div>
</div>
--}}
