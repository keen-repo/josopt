<?php

add_action("wp_ajax_update_cart", "update_cart");
add_action("wp_ajax_nopriv_update_cart", "update_cart");

function update_cart(  ) {
    global $woocommerce;
    $prodID = $_POST[ 'prod_id' ];
    $response[ 'id' ] = $prodID;

    $product_id = 208;
    $product_cart_id = WC()->cart->generate_cart_id( $product_id );
    $cart_item_key = WC()->cart->find_product_in_cart( $product_cart_id );

    if ( $cart_item_key ) {
        WC()->cart->remove_cart_item( $cart_item_key );
        $response[ 'type' ] = 'success';
    } else {
        $response[ 'type' ] = 'error';
    }
    $response[ 'log' ] = $cart_item_key;
    $response[ 'b' ] = $product_cart_id;
    $result = json_encode($response);
    echo $result;
    die(  );
}
