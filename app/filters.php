<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

add_filter( 'woocommerce_dropdown_variation_attribute_options_args', function( $args ) {
    global $product;
    $args['show_option_none'] = wc_attribute_label( $args[ 'attribute' ], $product );
    return $args;
} );

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

add_filter('sage/display_sidebar', function ($display) {
    static $display;

    isset($display) || $display = in_array(true, [
      // The sidebar will be displayed if any of the following return true
      //is_single(),
      is_404(),
      is_page_template('template-custom.php'),
      //is_shop()
    ]);

    return $display;
});

add_filter( 'get_the_archive_title', function ( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>' ;
    } elseif ( is_tax() ) { //for custom post types
        $title = sprintf( __( '%1$s' ), single_term_title( '', false ) );
    }
    return $title;
});

// Billing and shipping addresses fields
/*add_filter( 'woocommerce_default_address_fields' , function ( $address_fields ) {
    // Only on checkout page
    if ( ! is_checkout() ) {
        return $address_fields;
    }

    // All field keys in this array
    $key_fields = array('country','first_name','last_name','company','address_1','address_2','city','state','postcode');

    // Loop through each address fields (billing and shipping)
    foreach ( $key_fields as $key_field ) {
        $address_fields[$key_field]['required'] = false;
    }

    return $address_fields;
}, 20, 1 );*/

add_filter( 'woocommerce_default_address_fields', function( $fields ) {
    $fields['company']['priority'] = 15;
    $fields['country']['priority'] = 130;

    $fields['door_number']   = array(
        'label'        => '',
        'required'     => false,
        'class'        => array( 'form-row-wide', 'dg-door-number' ),
        'priority'     => 20,
        'placeholder'  => 'Door Number',
    );

    /**
     * Change LABELS and insert PLACEHOLDER
     */
    // NAME
    $fields['first_name']['label'] = '';
    $fields['first_name']['placeholder'] = 'Full Name*';

    $fields[ 'company' ][ 'label' ] = '';
    $fields[ 'company' ][ 'placeholder' ] = 'Company Name*';

    $fields[ 'country' ][ 'label' ] = '';
    $fields[ 'country' ][ 'placeholder' ] = 'Country*';

    $fields[ 'address_1' ][ 'label' ] = '';
    $fields[ 'address_1' ][ 'placeholder' ] = 'Address Line 1*';

    $fields[ 'address_2' ][ 'label' ] = '';
    $fields[ 'address_2' ][ 'placeholder' ] = 'Address Line 2*';

    $fields[ 'city' ][ 'label' ] = '';
    $fields[ 'city' ][ 'placeholder' ] = 'City*';

    $fields[ 'state' ][ 'label' ] = '';
    $fields[ 'state' ][ 'placeholder' ] = 'City*';

    $fields[ 'postcode' ][ 'label' ] = '';
    $fields[ 'postcode' ][ 'placeholder' ] = 'Postcode';

    $fields[ 'vat' ][ 'label' ] = '';
    //$fields[ 'vat' ][ 'placeholder' ] = 'VAT No.';

    $fields[ 'phone' ][ 'priority' ] = 4;

    return $fields;
} );

add_filter( 'woocommerce_shipping_fields', function( $fields ) {
    unset($fields['shipping_last_name']);
    //unset($fields['shipping_company']);
    //unset($fields['shipping_city']);

    return $fields;
} );

add_filter( 'woocommerce_billing_fields', function( $fields ) {
    unset($fields['billing_last_name']);
    //unset($fields['billing_company']);
    //unset($fields['billing_city']);

    /**
     * Set field order
     */
    //$fields['billing_phone']['priority'] = 17;
    //$fields['billing_postcode']['priority'] = 150;

    /**
     * Change LABELS and insert PLACEHOLDER
     */
    // Email
    $fields['billing_email']['label'] = '';
    $fields['billing_email']['placeholder'] = 'Email*';

    // Phone
    $fields['billing_phone']['label'] = '';
    $fields['billing_phone']['placeholder'] = 'Phone*';

    $fields['billing_phone']['priority'] = 17;

    return $fields;
}, 10, 1 );

add_filter( 'woocommerce_checkout_fields', function( $fields ) {
    //unset($fields['billing']['billing_company']);// Remove Company name - always
    //unset($fields['billing']['billing_shipping']);// I have no idea where this came from
    //unset($fields['shipping']['shipping_last_name']);
    //unset($fields['billing']['billing_country']);
    //unset($fields['shipping']['shipping_country']);

    $fields['billing']['billing_email']['priority'] = 16;
    //$fields['billing']['billing_phone']['priority'] = 17;
    $fields['billing']['billing_postcode']['priority'] = 140;

    $fields[ 'order' ][ 'order_comments' ][ 'label' ] = '';
    $fields[ 'order' ][ 'order_comments' ][ 'placeholder' ] = 'Note for delivery';

    return $fields;
}, 10, 1 );


add_action( 'woocommerce_form_field_tex', function( $field, $key ) {
    // will only execute if the field is billing_company and we are on the checkout page...
    if ( is_checkout() && ( $key == 'billing_door_number' ) ) {
        //echo '<pre>';
        //var_dump( $key );
        //var_export( $field );
        //echo '</pre>';
        $field .= '<div id="add_custom_heading"><h2>' . __('Custom Heading Here') . '</h2></div>';
    }
    return $field;
}, 10, 2 );

add_filter('woocommerce_billing_fields', function( $fields ) {

    $fields['billing_vat'] = array(
        'label' => __('VAT No.', 'woocommerce'), // Add custom field label
        'placeholder' => _x('VAT No.', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required' => false, // if field is required or not
        'clear' => false, // add clear or not
        'type' => 'text', // add field type
        'class' => array('keen_vat_no')    // add class name
    );

    return $fields;
} );

add_filter( 'woocommerce_shipping_package_name', function( $name ) {
  return 'Home Delivery';
} );
