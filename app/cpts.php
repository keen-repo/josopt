<?php
add_action( 'init', function() {
    register_extended_taxonomy(
        'collection',
        'product',
        [
            # Use radio buttons in the meta box for this taxonomy on the post editing screen:
            'meta_box' => 'radio',
            # Show this taxonomy in the 'At a Glance' dashboard widget:
            'dashboard_glance' => true,
        ],
        [
            # Override the base names used for labels:
            'singular' => 'Collection',
            'plural'   => 'Collections',
            'slug'     => 'collections'
        ]
    );
} );
